
resource "kubernetes_config_map" "nap_api_policy" {
  metadata {
    name      = "nap-api-policy"
    namespace = "default"
  }

  data = {
    "NAP_API_Policy.json" = file("./NAP_API_Policy.json")
  }
}

resource "kubernetes_config_map" "nginx_conf_map_api" {
  metadata {
    name      = "nginx-conf-map-api"
    namespace = "default"
  }

  data = {
    "nginx.conf" = file("./NGINX_API_Conf")
  }
}

resource "kubernetes_deployment" "api_nap" {
  metadata {
    name = "api-nap"

    labels = {
      app = "api-nap"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "api-nap"
      }
    }

    template {
      metadata {
        labels = {
          app = "api-nap"
        }
      }

      spec {
        volume {
          name = "nginx-conf-map-api-volume"

          config_map {
            name = "nginx-conf-map-api"
          }
        }

        volume {
          name = "nap-api-policy-volume"

          config_map {
            name = "nap-api-policy"
          }
        }

        container {
          name  = "api-nap"
          image = <<NGINX App Protect WAF image repository>>

          port {
            container_port = 80
          }

          volume_mount {
            name       = "nginx-conf-map-api-volume"
            read_only  = true
            mount_path = "/etc/nginx/nginx.conf"
            sub_path   = "nginx.conf"
          }

          volume_mount {
            name       = "nap-api-policy-volume"
            read_only  = true
            mount_path = "/etc/nginx/NAP_API_Policy.json"
            sub_path   = "NAP_API_Policy.json"
          }

          image_pull_policy = "IfNotPresent"
        }

        image_pull_secrets {
          name = <<Pull secret for your NGINX App Protect WAF image>>
        }
      }
    }
  }
}

resource "kubernetes_service" "api_nap" {
  metadata {
    name = "api-nap"

    labels = {
      app = "api-nap"

      service = "api-nap"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 80
      target_port = "80"
    }

    selector = {
      app = "api-nap"
    }

    type = "NodePort"
  }
}

resource "kubernetes_config_map" "nap_web_policy" {
  metadata {
    name      = "nap-web-policy"
    namespace = "default"
  }

  data = {
    "NAP_Web_Policy.json" = file("./NAP_Web_Policy.json")
  }
}

resource "kubernetes_config_map" "nginx_conf_map_web" {
  metadata {
    name      = "nginx-conf-map-web"
    namespace = "default"
  }

  data = {
    "nginx.conf" = file("./NGINX_Web_Conf")
  }
}

resource "kubernetes_deployment" "web_nap" {
  metadata {
    name = "web-nap"

    labels = {
      app = "web-nap"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "web-nap"
      }
    }

    template {
      metadata {
        labels = {
          app = "web-nap"
        }
      }

      spec {
        volume {
          name = "nginx-conf-map-web-volume"

          config_map {
            name = "nginx-conf-map-web"
          }
        }

        volume {
          name = "nap-web-policy-volume"

          config_map {
            name = "nap-web-policy"
          }
        }

        container {
          name  = "web-nap"
          image = <<NGINX App Protect WAF image repository>>

          port {
            container_port = 80
          }

          volume_mount {
            name       = "nginx-conf-map-web-volume"
            read_only  = true
            mount_path = "/etc/nginx/nginx.conf"
            sub_path   = "nginx.conf"
          }

          volume_mount {
            name       = "nap-web-policy-volume"
            read_only  = true
            mount_path = "/etc/nginx/NAP_Web_Policy.json"
            sub_path   = "NAP_Web_Policy.json"
          }

          image_pull_policy = "IfNotPresent"
        }

        image_pull_secrets {
          name = <<Pull secret for your NGINX App Protect WAF image>>
        }
      }
    }
  }
}

resource "kubernetes_service" "web_nap" {
  metadata {
    name = "web-nap"

    labels = {
      app = "web-nap"

      service = "web-nap"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 80
      target_port = "80"
    }

    selector = {
      app = "web-nap"
    }

    type = "NodePort"
  }
}
