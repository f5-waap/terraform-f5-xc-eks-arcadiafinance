resource "volterra_app_firewall" "waap-tf" {
  name                     = format("%s-waf", local.name)
  description              = format("WAF in block mode for %s", local.name)
  namespace                = local.namespace

  // One of the arguments from this list "allow_all_response_codes allowed_response_codes" must be set
  allow_all_response_codes = true
  // One of the arguments from this list "default_anonymization custom_anonymization disable_anonymization" must be set
  default_anonymization = true
  // One of the arguments from this list "use_default_blocking_page blocking_page" must be set
  use_default_blocking_page = true
  // One of the arguments from this list "default_bot_setting bot_protection_setting" must be set
  default_bot_setting = true
  // One of the arguments from this list "default_detection_settings detection_settings" must be set
  default_detection_settings = true
  // One of the arguments from this list "use_loadbalancer_setting blocking monitoring" must be set
  use_loadbalancer_setting = true
  // Blocking mode - optional - if not set, policy is in MONITORING
  blocking = true

}


resource "volterra_origin_pool" "api_nap" {
  depends_on = [kubernetes_service.api_nap]
  name                   = format("%s-api-nap-pool", local.name)
  namespace              = local.namespace
  description            = format("Origin pool pointing to API NGINX App Protect WAF k8s service running on CE")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "api-nap.default"
      site_locator {
        site {
          name      = local.f5_xc_aws_vpc_site
          namespace = "system"
        }
      }
    }
  }

  port               = 80
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
}

resource "volterra_origin_pool" "web_nap" {
  depends_on = [kubernetes_service.web_nap]
  name                   = format("%s-web-nap-pool", local.name)
  namespace              = local.namespace
  description            = format("Origin pool pointing to Web App NGINX App Protect WAF k8s service running on CE")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "web-nap.default"
      site_locator {
        site {
          name      = local.f5_xc_aws_vpc_site
          namespace = "system"
        }
      }
    }
  }

  port               = 80
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
}

resource "volterra_http_loadbalancer" "arcadia-finance" {
  depends_on = [volterra_origin_pool.web_nap, volterra_origin_pool.api_nap, volterra_app_firewall.waap-tf]
  name                            = "arcadia-finance-lb"
  namespace                       = local.namespace
  description                     = format("HTTP loadbalancer object for Arcadia Finance")
  domains                         = [var.app_fqdn]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-kibana[each.key].name
#      namespace = local.namespace
#    }
#  }

#  https_auto_cert {
#    add_hsts      = var.enable_hsts
#    http_redirect = var.enable_redirect
#    no_mtls       = true
#  }

  http {
    dns_volterra_managed = true
  }

  routes {
    simple_route {
      path {
        path = "/api/rest/execute_money_transfer.php"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.api_nap.name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }
  routes  {
    simple_route {
      path {
        path = "/trading/rest/buy_stocks.php"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.api_nap.name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }
  routes  {
    simple_route {
      path {
        path = "/trading/rest/sell_stocks.php"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.api_nap.name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }
  routes  {
    simple_route {
      path {
        prefix = "/"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.web_nap.name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }


  app_firewall {
    name = format("%s-waf", local.name)
    namespace = local.namespace
  }

  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true
  no_challenge                    = true
}
